import React from 'react'

import Categories from './home/Categories'
import Contact from './home/Contact'

export default function Home() {
  return (
    <>
      <Categories />
      <Contact />
    </>
  )
}
